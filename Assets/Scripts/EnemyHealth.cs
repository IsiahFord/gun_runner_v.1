﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {

    public int startingHealth = 100;
    public int currentHealth;
    public int scoreValue = 10;

    bool isDead;

    CapsuleCollider capsuleCollider;

	void Awake ()
    {
        currentHealth = startingHealth;

        capsuleCollider = GetComponentInChildren<CapsuleCollider>();
        isDead = false;
	}

    public void EnemyTakeDamage(int amount)
    {
        if (isDead)
            return;

        currentHealth -= amount;

        if (currentHealth <= 0)
        {
            EnemyDead();
        }
    }

    void EnemyDead()
    {
        isDead = true;

        capsuleCollider.isTrigger = true;

        Destroy(gameObject, 1f);

        ScoreManager.score += scoreValue;
    }
}
