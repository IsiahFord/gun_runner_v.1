﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {
    /*  Spawn 1 enemy at the begin, then increase the enemies dificulty and spawn more as time goes on.
        Enimies will be limited to a fixed amount able to be in the game at a time.
        They will spawn randomly between left, up and right of floor on a floating base.
        They may also spawn on the floor.
    */

   public GameObject enemyObj;

    public Vector3 spawnArea;

    //dist from player
    
   

    // Use this for initialization

    void Awake()
    {
       
      
    }
    void Update ()
    {
        if (Input.GetKeyDown(KeyCode.K))
            SpawnEnemy();      
    }
	
	// Update is called once per frame
	void SpawnEnemy ()
    {
        float randomX = Random.Range(-2, 2);
        float randomY = Random.Range(-2, 2);
        float randomZ = Random.Range(-2, 2);

        spawnArea = new Vector3(randomX,randomY,randomZ);
        Instantiate(enemyObj, spawnArea, transform.rotation);
	}
}
