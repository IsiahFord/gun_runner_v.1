﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public Transform target;

    Vector3 offset;

	void Start ()
    {
         
        offset = transform.position - target.position;
	}
	
	void FixedUpdate ()
    {
        transform.position = target.position + offset;

       // transform.position =new Vector3 (transform.position, targetCamPos, Time.deltaTime);
    }
}
