﻿using UnityEngine;
using System.Collections;

public class FloorSpawn : MonoBehaviour
{
    public GameObject currentFloor;
    public GameObject floorPrefab;

    // Use this for initialization
    void Start()
    {
        for (int i = 0; i < 15; i++)
        {
            SpawnFloor();
        }
    }

    public void SpawnFloor()//Spawn Floor
    {
        currentFloor = (GameObject)Instantiate(floorPrefab, currentFloor.transform.GetChild(1).transform.GetChild(0).position, Quaternion.identity);
    }
}
