﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    //Movment
    public float forwardSpeed;
    private Rigidbody rBody;    
    //Referances
    private FloorSpawn floorSpawn;
    //private Animator anim;
    //private bool walking;
  
	void Awake ()
    {
       // rBody = GetComponent<Rigidbody>();
        floorSpawn = GameObject.FindGameObjectWithTag("GameManager").GetComponent<FloorSpawn>();
        //anim = GetComponent<Animator>();
        //navMeshAgent = GetComponet<NavMeshAgent>();

    }
	
	void FixedUpdate ()
    {
        Move();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "SpawnTrigger")
        floorSpawn.SpawnFloor();
    }

    void Move()
    {
        transform.Translate(Vector3.forward * forwardSpeed * Time.deltaTime);
    }
}
/*
  void Update()
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // if (Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out hit))
        //ray = gameObject.GetComponentInChildren<Camera>().ScreenPointToRay(Input.mousePosition);


        if (Input.GetButtonDown("Fire2"))
        {
            if (Physics.Raycast(ray, out hit, 10000, enemyMask))
            {
                if (hit.collider.CompareTag("Enemy"))
                {
                    targetEnemy = hit.transform; //target enemy is the object hit ith the ray
                    enemyClicked = true; //An 'enemy' has been clicked
                }
                else
                {
                   // walking = true;
                    enemyClicked = false; //An enemy has not been clicked
                   // navMeshAgent.destionation = hit.point; //move to ray hit position
                   // navMeahAgent.Resume();
                }
            }         
        }
        */