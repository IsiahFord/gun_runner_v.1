﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {

    /*Camera ray shoots at objects in scene indesrimanantly
      The "gunRay" will then shoot at the spot hit by "camRay"
      If "gunRay" hits an "Enemy" it will shoot,
      Else RETURN
    */

    public int damagePerShot = 100; //Damage/Power of shot
    public float timeBetweenBullets = 0.5f; //Fire rate
    public float shootDistance = 10000f; //Range of shot

    float timer;
    float effectsDispalyTime = 0.2f;
    //Gun
    Ray shootRay;
    RaycastHit shootHit;
    //Cam
    Ray camRay;
    RaycastHit camRayHit;

    private bool enemyClicked; //Was enemy clicked
    public LayerMask enemyMask;

    //Refferances
    private Transform targetEnemy; //Tageted enemy
    public GameObject gun;
    public GameObject gunBarrel;
    public Vector3 cam;

    ParticleSystem gunParticle;
    Light gunLight;
    AudioSource gunSound;
    LineRenderer gunLine;

    //public GameObject firePoint;

   
    void Awake ()
    {
        gunParticle = GetComponent<ParticleSystem>();
        gunLight = GetComponent<Light>();
        //fireSound = GetComponent<AudioSource>();
        cam = Camera.main.transform.position;
        gunLine = GetComponent<LineRenderer>();
    }

    void Update()
    {
        CameraRayAim();
        

        timer += Time.deltaTime; //Start from 0 and increase in real seconds

        Shoot();

        if (timer >= timeBetweenBullets * effectsDispalyTime) //'timer' has been reset at 0, through shoot function. If .15 * .2 = .03) .03 sec has passed execute bellow...
               DisableEffects(); //Call 'DisableEffects' function
    }

    void FixedUpdate()
    {
     //   CameraRayAim();
    }

    public void DisableEffects()
    {
        gunLight.enabled = false; //Disable the 'light' component
        gunLine.enabled = false; //Disable 'partical system' compnent
    }

    void CameraRayAim()
    {
       camRay = Camera.main.ScreenPointToRay(Input.mousePosition); //'camRay' end point is that of mouse point on screen
        Debug.DrawLine(cam, shootHit.point, Color.red, 12f);
    }

    void Shoot() //'Shoot' function. When called it will exaqute the code in it
    {
        shootRay = camRay; //'shootRay' will aim at 'camRay'

        if (Input.GetButtonDown("Fire2") && timer >= timeBetweenBullets) //If fire button is preassed and 'timer' has elapsed the 'timeBetweenBullets' do the following...
        {
            timer = 0f; //reset the timer

            
            //Fix This ***
           

            //gunSound.Play()

            gunLight.enabled = true; //Turn on 'light' component to simulate a muzzle flash

            gunParticle.Stop(); //Stop an playback to insure only one 'gunParticle' is playing, then...
            gunParticle.Play(); //... play the particle

            gunLine.enabled = true; //Turn on the line renderer
            gunLine.SetPosition(0, transform.position); //Set the start of the line(start index(0), position) to be the 'gunbarrel' (this Gameobject) 

            if (Physics.Raycast(shootRay, out shootHit, shootDistance, enemyMask))
            {
                EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();

                if (enemyHealth != null)
                {
                    Debug.Log("Got EnemyHealth script");
                    Debug.DrawLine(transform.position, shootHit.point, Color.blue, 5f);   //...draw line from  transform of 'this' game object (GunBarrel) to hit point
                    enemyHealth.EnemyTakeDamage(damagePerShot);
                }
               // gun.transform.LookAt(shootRay.direction); //Look at hit point caused by camera then...
                gunLine.SetPosition(1, shootHit.point);
            }
            else
            {
               // gun.transform.LookAt(shootRay.direction); //Look at hit point caused by camera then...
                //Debug.DrawLine(transform.position, Input.mousePosition, Color.yellow, 7f);
                gunLine.SetPosition(1, shootRay.origin + shootRay.direction * shootDistance);
            }
        }
    }
}
