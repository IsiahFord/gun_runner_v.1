﻿using UnityEngine;
using System.Collections;

public class EnemyShootRaycast : MonoBehaviour {

    public EnemyWeapon weapon;

    public GameObject firePoint;

    public Transform player;

    [SerializeField] //Allows private fields to be seen in inspector
    private LayerMask mask;

    float distFromPlayer;
    public Vector3 playerV;

    void Start()
    {
       /* playerV = GameObject.Find("Player").transform.position;
        distFromPlayer = Vector3.Distance(playerV, transform.position);
        print("Distance to other: " + distFromPlayer);
        Debug.DrawLine(playerV, transform.position, Color.green, 20f);
        */
    }

    void Update()
    {
        //if Player in range look at Player..
        TargetPlayer();
        //and wait time over Fire
         EnemyFire();
    }

    void TargetPlayer()
    {
        transform.LookAt(player);
    }

    void EnemyFire()
    {
        RaycastHit _hit;
        if (Physics.Raycast(firePoint.transform.position, firePoint.transform.forward, out _hit,weapon.range, mask)) //start, direction, hit info, range, mask
        {
            Debug.Log("Enemy Hit " + _hit.collider.name);
            Debug.DrawLine(firePoint.transform.position, _hit.point, Color.red, 6f); //oragin, hit point, color of line, duraition
        }
    }
}
